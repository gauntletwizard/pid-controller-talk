from locust import HttpLocust, TaskSet, task

class TestSlow(TaskSet):
    @task
    def slow(self):
        self.client.get("/slow")


class PIDTester(HttpLocust):
    task_set = TestSlow
