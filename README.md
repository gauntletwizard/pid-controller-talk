This is the repo for Thomas Hahn and Mark Hahn's talk "Control Theory for SRE", which demonstrates implementing a PID controller using Prometheus in your kubernetes cluster, then autoscaling your deployment in response.

This tutorial expects you to already have a functional Kubernetes cluster. It will create a Namespace with a Prometheus (and associated extras) deployment, as well as our example service deployment, and a loadstester for that example. You may need extra capacity in your cluster - If you have already installed and setup cluster-autoscaler, you should already be set. If no, it is recommended to simply scale a few extra nodes for the duration of this exercise.

Steps for running:
1) Install Prometheus and extras (Grafana, Custom Metric Adapter)
2) Install our deployment
3) Install our loadtester
4) Loadtest
5) Setup Prometheus Rules
6) Loadtest again
