package main

import (
	"math"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// waveCollector is a prometheus.Collector that reports the current wave.
type waveCollector struct{}

var waveCollectorDesc = prometheus.NewDesc("wave_a", "A metric that forms a sine wave", []string{}, nil)

func init() {
	prometheus.MustRegister(waveCollector{})
}

func (t waveCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(t, ch)
	// c <- waveCollectorDesc
}

func (t waveCollector) Collect(ch chan<- prometheus.Metric) {
	ch <- prometheus.MustNewConstMetric(
		waveCollectorDesc,
		prometheus.GaugeValue,
		math.Sin(float64(time.Now().Unix())*math.Pi/3600),
	)
}
