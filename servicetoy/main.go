package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	a_gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "a_value",
		Help: "Current value of A",
	})
	a_summary = promauto.NewSummary(prometheus.SummaryOpts{
		Name: "a_summary",
		Help: "Summary of the value of A",
	})
	a_histogram = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "a_histogram",
		Help:    "Histogram of historical A values",
		Buckets: CenteredBuckets(.00001, 10, 10),
	})
	all_sets = promauto.NewCounter(prometheus.CounterOpts{
		Name: "all_sets",
		Help: "Count of calls to '/set'",
	})
	a_sets = promauto.NewCounter(prometheus.CounterOpts{
		Name: "a_sets",
		Help: "Count of calls to '/set' containing an a query",
	})
	b_sets = promauto.NewCounter(prometheus.CounterOpts{
		Name: "b_sets",
		Help: "Count of calls to '/set' containing a b query",
	})
)

func CenteredBuckets(start, factor float64, count int) []float64 {
	topBuckets := prometheus.ExponentialBuckets(start, factor, count)
	bottomBuckets := make([]float64, len(topBuckets), len(topBuckets))
	for i := range topBuckets {
		bottomBuckets[len(topBuckets)-i-1] = -topBuckets[i]
	}
	return append(bottomBuckets, topBuckets...)
}

type SimpleServer struct {
	a int64
	b string
}

func (s *SimpleServer) Set(w http.ResponseWriter, r *http.Request) {
	all_sets.Inc()
	r.ParseForm()
	a, err := strconv.ParseInt(r.Form.Get("a"), 10, 64)
	if err == nil {
		a_sets.Inc()
		s.a = a
		a_gauge.Set(float64(a))
		a_summary.Observe(float64(a))
		a_histogram.Observe(float64(a))
		fmt.Fprintln(w, "Setting A to", a)
	}
	if b, ok := r.Form["b"]; ok {
		b_sets.Inc()
		s.a = a
		s.b = b[0]
		fmt.Fprintln(w, "Setting B to", b)
	}
}

func (s *SimpleServer) Get(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "A is set to", s.a)
	fmt.Fprintln(w, "B is set to", s.b)
}

func main() {
	var s SimpleServer
	Handle("/set", http.HandlerFunc(s.Set))
	HandleFunc("/get", s.Get)

	Handle("/slow", newSlowHandler())
	Handle("/burn", newBurnHandler())

	Handle("/control", newControlHandler())
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
	}
}
