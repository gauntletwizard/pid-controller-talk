package main

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// timeCollector is a prometheus.Collector that reports the current time.
type timeCollector struct{}

var timeCollectorDesc = prometheus.NewDesc("current_time", "The current time", []string{}, nil)

func init() {
	prometheus.MustRegister(timeCollector{})
}

func (t timeCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(t, ch)
	// c <- timeCollectorDesc
}

func (t timeCollector) Collect(ch chan<- prometheus.Metric) {
	ch <- prometheus.MustNewConstMetric(
		timeCollectorDesc,
		prometheus.CounterValue,
		float64(time.Now().Unix()),
	)
}
