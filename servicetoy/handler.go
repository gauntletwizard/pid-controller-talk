package main

import (
	"math"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// A constant for the label that our http.Handle pattern maps to.
const patternLabel = "pattern"

var (
	// http_status_codes is used as an argument for promhttp.InstrumentHandlerCounter to count status code responses.
	// It should use special status code 599 to represent a panic
	http_status_codes = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_status_codes",
			Help: "Number of responses for each HTTP Status Code",
		},
		[]string{patternLabel, "code", "method"},
	)

	http_response_time = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_response_time",
			Help:    "Histogram of Response Times",
			Buckets: prometheus.ExponentialBuckets(math.Pow(2, -10), 2, 20),
		},
		[]string{patternLabel, "code", "method"},
	)

	http_requests_inflight = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "http_requests_inflight",
			Help: "Requests in flight",
		},
		[]string{patternLabel},
	)
)

func Handle(pattern string, handler http.Handler) {
	l := prometheus.Labels{patternLabel: pattern}
	nextHandler := handler
	nextHandler = promhttp.InstrumentHandlerCounter(http_status_codes.MustCurryWith(l), nextHandler)
	nextHandler = promhttp.InstrumentHandlerDuration(http_response_time.MustCurryWith(l), nextHandler)
	nextHandler = promhttp.InstrumentHandlerInFlight(http_requests_inflight.With(l), nextHandler)
	http.Handle(pattern, nextHandler)
}

func HandleFunc(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	Handle(pattern, http.HandlerFunc(handler))
}
