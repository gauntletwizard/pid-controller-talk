package main

import (
       "fmt"
       "strconv"
	"net/http"
	"time"
)

// A slowHandler is a function that is normally fast, but slows down in response to lots of traffic.
type burnHandler struct {
	tickchan <-chan time.Time
}

func newBurnHandler() *burnHandler {
	return &burnHandler{tickchan: time.Tick(50 * time.Millisecond)}
}

// http://localhost:8080/burn?count=12
func (s burnHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    var count = 1
    var debugPrint = false

    if(debugPrint) {
        fmt.Printf("%s\n", r.URL)
    }
    var q = r.URL.Query()
    if(debugPrint) {
        fmt.Println("Query")
        fmt.Println()
    }


    keys, ok := r.URL.Query()["count"]
    if(debugPrint) {
        fmt.Println("key, count")
        fmt.Println(q)
        fmt.Println("okay, count")
        fmt.Println(ok)
    }

    if !ok || len(keys[0]) < 1 {
        if(debugPrint) {
            fmt.Println("Url Param 'count' is missing")
	}
	count = 3	
    } else {
        if(debugPrint) {
            fmt.Println("Param 'count'")
	    fmt.Println(keys)
            fmt.Println(keys[0]);
	}
	var countStr = keys[0]

	x, err := strconv.Atoi(countStr)
        if(debugPrint) {
            fmt.Println("x")
            fmt.Println(x)
        }

	if err != nil {
            if(debugPrint) {
                fmt.Println("error parsing")
            }
            x = 3
        }
        count = x
    }

    if(debugPrint) {
        fmt.Println("final count")
        fmt.Println(count)
        fmt.Println("okay")
        fmt.Println(ok)
    }

    if (count < 0) {
        count = -1 * count
    }

    var expr = 0.0
    var countDown = count
    for countDown > 1 {
        countDown -= 1
        expr += divide(countDown)
    }
    if(debugPrint) {
        fmt.Println("expr")
        fmt.Println(expr)
    }

    var exprStr = fmt.Sprintf("Result: %f", expr) 
    if(debugPrint) {
        fmt.Println("exprStr")
        fmt.Println(exprStr)
    }

    t := time.Now()
    var dateTimeStr = t.Format(time.RFC3339)

    var countStr = fmt.Sprintf("Count: %d", count);

    w.WriteHeader(200)
    w.Write([]byte(dateTimeStr))
    w.Write([]byte("\n"));
    w.Write([]byte(countStr))
    w.Write([]byte("\n"));
    w.Write([]byte(exprStr))
    w.Write([]byte("\n"));
}

func divide (n int) float64 {
    var divideCount = n

    var divideSum = 0.0
    for divideCount > 0 {

	divideSum += 1.0/float64(divideCount)
	
        divideCount -= 1
    }

    return divideSum
}
