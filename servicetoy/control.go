package main

import (
	"html/template"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type controlHandler struct {
	Kp       float64
	Ki       float64
	Kd       float64
	Setpoint float64
}

var (
	kp_metric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "control_kp",
		Help: "kp Term",
	})
	ki_metric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "control_ki",
		Help: "ki Term",
	})
	kd_metric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "control_kd",
		Help: "kd Term",
	})
	setpoint_metric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "control_setpoint",
		Help: "setpoint Term",
	})
)

func newControlHandler() *controlHandler {
	return &controlHandler{
		1,
		.1,
		.2,
		10,
	}
}

func (s *controlHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	if KpS := r.Form.Get("Kp"); KpS != "" {
		Kp, err := strconv.ParseFloat(KpS, 64)
		if err == nil {
			s.Kp = Kp
			kp_metric.Set(Kp)
		}
	}
	if KiS := r.Form.Get("Ki"); KiS != "" {
		Ki, err := strconv.ParseFloat(KiS, 64)
		if err == nil {
			s.Ki = Ki
			ki_metric.Set(Ki)
		}
	}
	if KdS := r.Form.Get("Kd"); KdS != "" {
		Kd, err := strconv.ParseFloat(KdS, 64)
		if err == nil {
			s.Kd = Kd
			kd_metric.Set(Kd)
		}
	}
	if setpointS := r.Form.Get("setpoint"); setpointS != "" {
		setpoint, err := strconv.ParseFloat(setpointS, 64)
		if err == nil {
			s.Setpoint = setpoint
			setpoint_metric.Set(setpoint)
		}
	}
	t.Execute(w, s)
}

var t = template.Must(template.New("foo").Parse(`
<html><head><title>Foo Bar</title></head>
	<body>
	<form>
	Kp: <input type="text" name="Kp" value="{{.Kp}}"/>
	Ki: <input type="text" name="Ki" value="{{.Ki}}"/>
	Kd: <input type="text" name="Kd" value="{{.Kd}}"/>
	setpoint: <input type="text" name="setpoint" value="{{.Setpoint}}"/>
	<input type="submit">
	</body>
</html>`))
