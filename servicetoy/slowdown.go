package main

import (
	"net/http"
	"time"
)

// A slowHandler is a function that is normally fast, but slows down in response to lots of traffic.
type slowHandler struct {
	tickchan <-chan time.Time
}

func newSlowHandler() *slowHandler {
	return &slowHandler{tickchan: time.Tick(50 * time.Millisecond)}
}

func (s slowHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	<-s.tickchan
	w.WriteHeader(200)
	w.Write([]byte("Foo"))
}
