FROM golang

RUN go get golang.org/x/tools/cmd/present
ADD . /home/present
USER 1001:1001

WORKDIR /home/present/present
ENTRYPOINT ["/go/bin/present", "-notes", "-http", ":3999"]
