package main

/* QueryScaleController is a hack. It takes a Prometheus query and scales a k8s deployment based on that value.
 */

import (
	"context"
	"flag"
	"fmt"
	"time"

	promapi "github.com/prometheus/client_golang/api"
	prom "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	// autoscaling "k8s.io/api/autoscaling/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/gauntletwizard/pid-controller-talk/prometheus/queryscalecontroller/k8s"
)

var endpoint = flag.String("endpoint", "http://prometheus.kube.gauntletwizard.net", "Prometheus endpoint")
var query = flag.String("query", "scalar(setpoint)", "Prometheus query that returns number of pods. Must be a scalar()")
var deployment = flag.String("deployment", "promtoy", "Deployment to scale")
var duration = flag.Duration("duration", time.Second*10, "Time between scaling actions")

func main() {
	flag.Parse()

	// Init prometheus client
	h, err := promapi.NewClient(promapi.Config{Address: *endpoint})
	if err != nil {
		fmt.Println(err)
	}
	c := prom.NewAPI(h)

	var pods int32

	// Init k8s client
	client, ns := k8s.NewK8sClient()
	for {
		// Every tick:
		// Grab prometheus scalar
		// Scale deployment
		// Profit!
		resp, warnings, err := c.Query(context.Background(), *query, time.Now())
		switch v := resp.(type) {
		case *model.Scalar:
			fmt.Println(v.Value)
			pods = int32(v.Value)
		default:
			fmt.Println("Bad query, %s must be a scalar. Wrap with 'scalar()' or check that inner query is a vector with cardinality 1. Using last scale")
		}
		if len(warnings) != 0 || err != nil {
			fmt.Println("Warnings:", warnings, err)
		}
		fmt.Println("Scaled deployment to ", pods)

		if pods == 0 {
			continue
		}
		scale, err := client.Apps().Deployments(ns).GetScale(*deployment, meta.GetOptions{})
		fmt.Println(scale, err)

		scale.Spec.Replicas = pods

		_, err = client.Apps().Deployments(ns).UpdateScale(*deployment, scale)

		fmt.Println(err)

		time.Sleep(*duration)
	}

}
